package com.devcamp.car_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.car_api.model.CCarType;

public interface ICarTypeRespository extends JpaRepository<CCarType, Integer>{
    
}
