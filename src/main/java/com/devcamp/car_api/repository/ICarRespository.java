package com.devcamp.car_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.car_api.model.CCar;

public interface ICarRespository extends JpaRepository<CCar, Integer> {
    CCar findByCarCode(String car);
}
