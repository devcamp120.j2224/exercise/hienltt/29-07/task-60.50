package com.devcamp.car_api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.car_api.model.CCar;
import com.devcamp.car_api.model.CCarType;
import com.devcamp.car_api.repository.ICarRespository;
@CrossOrigin(value = "*", maxAge = -1)
@RestController
@RequestMapping("/")
public class CarController {
    @Autowired
    ICarRespository iCarRespository;

    @GetMapping("/devcamp-cars")
    public ResponseEntity<List<CCar>> getCarList(){
        try {
            List<CCar> pCars = new ArrayList<>();
            iCarRespository.findAll().forEach(pCars :: add);
            return new ResponseEntity<List<CCar>>(pCars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-cartypes")
    public ResponseEntity<Set<CCarType>> getCarTypeByCarCode(@RequestParam(value = "carCode") String carCode){
        try {
            CCar vCar = iCarRespository.findByCarCode(carCode);

            if(vCar != null){
                return new ResponseEntity<>(vCar.getTypes(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
